//
//  OrderBookTableRow.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import WatchKit

class OrderBookTableRow: NSObject {
    @IBOutlet weak var priceLabel: WKInterfaceLabel!
    @IBOutlet weak var btcLabel: WKInterfaceLabel!
}
