//
//  ApiHandler.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//  Used BTCe Public API v3
//

import UIKit

public class ApiHandler{
    enum Requests: String {
        case BTC_TICKER = "https://btc-e.com/api/3/ticker/btc_usd"
        case BTC_ORDER_BOOK_10 = "https://btc-e.com/api/3/depth/btc_usd?limit=6"
    }
    
    public class func getResponseFromPublicServerUrl(request: String, result: NSDictionary? -> Void){
        let url = NSURL(string: request)
        let request = NSURLRequest(URL: url!)
        let queue:NSOperationQueue = NSOperationQueue.mainQueue()
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: { (response, data, error) in
            if (error != nil){
                print("NSURLConnection Error: \(error)")
                result(nil)
            }else{
                result(try! data!.getJsonFromNSData())
            }
        })
    }
    
    public class func getBuySellRate(result: (Double?, Double?) -> Void){
        getResponseFromPublicServerUrl(Requests.BTC_TICKER.rawValue, result: { json in
            if let _ = json {
                let ticker = json!["btc_usd"] as! NSDictionary
                if let buyRate = ticker["buy"] as? Double{
                    if let sellRate = ticker["sell"] as? Double{
                        result(buyRate, sellRate)
                    }
                }                
            }else{
                self.loadLastSavedRateValues(){ (buy, sell) in
                    result(buy, sell)
                }
            }
        })
    }
    
    public class func loadLastSavedRateValues(values: (Double?, Double?) -> Void) {
        let defaults = NSUserDefaults(suiteName: "group.YawzaBot.Widget")
        if let _ = defaults?.objectForKey("buyRate") as? String {
            values(defaults?.objectForKey("buyRate") as? Double, defaults?.objectForKey("sellRate") as? Double)
        }
    }
    
    public class func getOrderBook(result: (NSArray?, NSArray?) -> Void){
        getResponseFromPublicServerUrl(Requests.BTC_ORDER_BOOK_10.rawValue, result: { json in
            if let data = json {
                let ticker = data["btc_usd"] as! NSDictionary
                if let asks = ticker["asks"] as? NSArray{
                    if let bids = ticker["bids"] as? NSArray{
                        result(asks, bids)
                    }
                }
            }
        })
    }

}
