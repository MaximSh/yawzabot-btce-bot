//
//  Authentication.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 17/11/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit
import LocalAuthentication

public class Authentication: NSObject {
    private enum UDefaults: String{
        case AUTH_KEY = "YazwaBotPassword"
        case API_KEY = "YazwaBotApiKey"
        case API_SECRET = "YazwaBotApiSecret"
    }
    
    public class var apiKey: String? {
        get{
            return (NSUserDefaults.standardUserDefaults().stringForKey(UDefaults.API_KEY.rawValue))
        }
        set(newValue){
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: Authentication.UDefaults.API_KEY.rawValue)
        }
    }
    
    public class var secretKey: String? {
        get{
        return (NSUserDefaults.standardUserDefaults().stringForKey(UDefaults.API_SECRET.rawValue))
        }
        set(newValue){
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: Authentication.UDefaults.API_SECRET.rawValue)
        }
    }
    
    private enum AuthBehaviour: String {
        case AUTH_CANCELLED_SYSTEM = "Authentication was cancelled by the system"
        case AUTH_CANCELLED_USER = "Authentication was cancelled by the user"
        case AUTH_CUSTOM_PWD = "User selected to enter custom password"
        case AUTH_FAILED = "Authentication failed"
        case AUTH_NOT_ENROLLED = "TouchID is not enrolled"
        case AUTH_PASSCODE_NOT_SET = "A passcode has not been set"
        case AUTH_TOUCH_ID_NOT_AVAILABLE = "TouchID not available"
    }
    
    class func authenticateUser(controller: UIViewController) {
        let context = LAContext()
        var error: NSError?
        let reasonString = "Authentication is needed to access an application."
        
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
            [context .evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success: Bool, evalPolicyError: NSError?) -> Void in
                
                if !success {
                    // If authentication failed then show a message to the console with a short description.
                    // In case that the error is a user fallback, then show the password alert view.
                    print(evalPolicyError?.localizedDescription)
                    
                    switch evalPolicyError!.code {
                        
                    case LAError.SystemCancel.rawValue:
                        print(AuthBehaviour.AUTH_CANCELLED_SYSTEM.rawValue)
                        
                    case LAError.UserCancel.rawValue:
                        print(AuthBehaviour.AUTH_CANCELLED_USER.rawValue)
                        self.authenticateUser(controller)
                        
                    case LAError.UserFallback.rawValue:
                        print(AuthBehaviour.AUTH_CUSTOM_PWD.rawValue)
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            self.showPasswordAlert(controller)
                        })
                    default:
                        NSLog(AuthBehaviour.AUTH_FAILED.rawValue)
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            self.showPasswordAlert(controller)
                        })
                    }
                }
                
            })]
        }else{
            // If the security policy cannot be evaluated then show a short message depending on the error.
            switch error!.code{
                
            case LAError.TouchIDNotEnrolled.rawValue:
                print(AuthBehaviour.AUTH_NOT_ENROLLED.rawValue)
                
            case LAError.PasscodeNotSet.rawValue:
                print(AuthBehaviour.AUTH_PASSCODE_NOT_SET.rawValue)
                
            default:
                // The LAError.TouchIDNotAvailable case.
                print(AuthBehaviour.AUTH_TOUCH_ID_NOT_AVAILABLE.rawValue)
            }
            
            // Optionally the error description can be displayed on the console.
            print(error?.localizedDescription)
            
            // Show the custom alert view to allow users to enter the password.
            self.showPasswordAlert(controller)
        }
    }
    
    class func showPasswordAlert(controller: UIViewController) {
        let passwordAlertController = UIAlertController(title: "Touch ID Auth", message: "Please type your password", preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default){ (action) in
            self.authenticateUser(controller)
        }
        passwordAlertController.addAction(cancelAction)
        let okayAction = UIAlertAction(title: "Okay", style: .Default){ (action) in
            let passwordTextField = passwordAlertController.textFields![0] 
            let userPassword = NSUserDefaults.standardUserDefaults().objectForKey(UDefaults.AUTH_KEY.rawValue) as! String
            if !passwordTextField.text!.isEmpty {
                if passwordTextField.text == userPassword {
                    
                }
                else{
                    self.showPasswordAlert(controller)
                }
            }
            else{
                self.showPasswordAlert(controller)
            }
            
        }
        passwordAlertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Password"
            textField.secureTextEntry = true
        }
        passwordAlertController.addAction(okayAction)
        controller.presentViewController(passwordAlertController, animated: true, completion: nil)
    }
    
    class func userPasswordSet() -> Bool{
        return ((NSUserDefaults.standardUserDefaults().objectForKey(UDefaults.AUTH_KEY.rawValue)) != nil)
    }
    
    class func setUserPassword(password: String){
        NSUserDefaults.standardUserDefaults().setObject(password, forKey: Authentication.UDefaults.AUTH_KEY.rawValue)
    }
}
