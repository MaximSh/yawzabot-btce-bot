//
//  Order.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 05/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

public class Order {
    enum OrderType : String {
        case SELL = "sell"
        case BUY = "buy"
    }
    public var id: Int = 0
    public var rate: Double = 0.0
    public var amount: Double = 0.0
    public var total: Double = 0.0
    public var timestamp: Int = 0
    var type: OrderType?
    
    init(id: Int, rate: Double, amount: Double, timestamp: Int = 0 , type: OrderType){
        self.id = id
        self.rate = rate
        self.amount = amount
        self.timestamp = timestamp
        self.type = type
        self.total = amount * rate
    }
    
    class func checkType(type: String) -> OrderType{
        return type == OrderType.SELL.rawValue ? OrderType.SELL : OrderType.BUY
    }
    
    class func getColorForType(type: OrderType) -> UIColor{
        if type == OrderType.SELL{
            return UIColor.redColor()
        }else{
            return UIColor.greenColor()
        }
    }
}
